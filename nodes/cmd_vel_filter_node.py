#!/usr/bin/env python2
# Copyright (c) 2019, Rick Armstrong
# All rights reserved.
#
# License:
# https://bitbucket.org/rick_armstrong/cmd_vel_filter/src/master/LICENSE
#
import threading

from geometry_msgs.msg import Twist
from nav_msgs.msg import OccupancyGrid, Path
import numpy as np
import rospy
from rospy_message_converter.message_converter import convert_ros_message_to_dictionary as msg_to_dict
import tf

from cmd_vel_filter.filter import collision_is_imminent
from cmd_vel_filter.trajectory import calc_trajectory, make_path, min_samples
import cmd_vel_filter.transformations as xf


class CmdVelFilter(threading.Thread):
    def __init__(
            self,
            cmd_vel_in,
            cmd_vel_out,
            costmap_topic,
            lookahead_seconds,
            base_frame_id,
            costmap_frame_id):
        self.lookahead_s = lookahead_seconds
        self.costmap_sub = rospy.Subscriber(
            costmap_topic,
            OccupancyGrid,
            self.costmap_cb)
        self.base_frame = base_frame_id
        self.costmap_frame = costmap_frame_id
        self.cmd_vel_sub = rospy.Subscriber(cmd_vel_in, Twist, self.cmd_vel_cb)
        self.filtered_cmd_vel_pub = rospy.Publisher(
            cmd_vel_out,
            Twist,
            queue_size=10)
        self.path_pub = rospy.Publisher('~path', Path, queue_size=10)
        self.tl = tf.TransformListener()

        # Costmap data, initialized after start.
        self.costmap = None
        self.costmap_metadata = None
        threading.Thread.__init__(self)

    def cmd_vel_cb(self, msg):
        if self.costmap is None:
            # We're blind, so don't pass on the incoming cmd_vel.
            rospy.logdebug("No costmap, dropping cmd_vel message.")
            return

        # Only proceed if we have a valid transform between the costmap's
        # global_frame, and the base_frame.
        try:
            self.tl.waitForTransform(
                self.costmap_frame,
                self.base_frame,
                rospy.Time(),
                rospy.Duration(3))
        except tf.Exception:
            err_msg = ("No transform from base to costmap frame available,"
                       "dropping cmd_vel message.")
            rospy.logdebug(err_msg)
            return

        # Find the odom->base_link angle.
        trans, rot = self.tl.lookupTransform(
            self.costmap_frame,
            self.base_frame,
            rospy.Time())
        theta = xf.euler_from_quaternion(rot, 'rzyx')[2]

        # Calculate the number of points needed to predict the safety of our
        # commanded velocity. We must sample densely-enough to check every
        # grid cell in the costmap that intersects the arc of the proposed
        # trajectory. We must choose a sampling density that is <= the costmap
        # resolution (see Nyquist). We choose 3X for good measure.
        v = msg.linear.x
        w = msg.angular.z
        t = self.lookahead_s
        n = min_samples(v, t, self.costmap.info.resolution)

        if n > 0:
            # Publish the projected trajectory (for visualization purposes).
            points, orientations = calc_trajectory(v, w, t, n)
            timestamp = rospy.Time.now()
            path = make_path(points, orientations, timestamp, 'base_link')
            self.path_pub.publish(path)

            # Decide if the incoming cmd_vel might cause a collision.
            # If safe, publish it, else drop it.
            if not collision_is_imminent(points, self.costmap, theta):
                self.filtered_cmd_vel_pub.publish(msg)

    def costmap_cb(self, msg):
        self.costmap = msg

    def run(self):
        try:
            while not rospy.is_shutdown():
                rospy.loginfo("cmd_vel_filter started.")
                rospy.spin()
        except rospy.exceptions.ROSInterruptException:
            pass
        rospy.loginfo("CmdVelFilter.run() exiting.")


if __name__ == '__main__':
    rospy.init_node('cmd_vel_filter')
    inn = rospy.get_param('in', '~in')
    out = rospy.get_param('out', '/cmd_vel')
    cmt = rospy.get_param(
        'costmap_topic',
        '/move_base/local_costmap/costmap')
    lookahead_s = rospy.get_param('lookahead_s', 7.0)
    base_frame = rospy.get_param(
        '/move_base/local_costmap/robot_base_frame',
        'base_link')
    costmap_frame = rospy.get_param(
        '//move_base/local_costmap/global_frame',
        'odom')
    cvf = CmdVelFilter(inn, out, cmt, lookahead_s, base_frame, costmap_frame)
    cvf.start()
    rospy.spin()
