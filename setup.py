from distutils.core import setup

setup(name='cmd_vel_filter',
      version='0.1.0',
      description='ROS node that filters possibly-dangerous velocity commands .',
      url='https://rick_armstrong@bitbucket.org/rick_armstrong/cmd_vel_filter.git',
      author='Rick Armstrong',
      author_email='waitingfortheelectrician@gmail.com',
      license='BSD 3-clause',
      packages=['cmd_vel_filter'],
      package_dir={'': 'src'}
      )
