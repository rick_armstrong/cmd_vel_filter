# Copyright (c) 2019, Rick Armstrong
# All rights reserved.
#
# License:
# https://bitbucket.org/rick_armstrong/cmd_vel_filter/src/master/LICENSE
#
from math import ceil, fabs

from geometry_msgs.msg import Point, Pose, PoseStamped, Quaternion
from nav_msgs.msg import Path
from std_msgs.msg import Header
import numpy as np
import transformations as xf


def calc_trajectory(l_vel, a_vel, duration_s, n_samples):
    """
    Return a list of 2D poses (Point(x, y, z), Quaternion(w, x, y, z)),
    calculated assuming a circular trajectory.

    Args:
        l_vel (float): linear velocity, possibly negative.
        a_vel (float): angular velocity.
        duration_s (float): length of time to extrapolate,
            from the current moment.
        n_samples (int): number of trajectory points to generate along the path.

    Returns:
        trajectory (list[((3-tuple), (4-tuple))]): a list of poses, where the
            3-tuple is a point (x, y, z), and the 4-tuple is an orientation, in
            the form of a quaternion (w, x, y, z). Points are expressed
            in the robot-fixed, robot-centric coordinate system, x->forward,
            y->left, z->up.
    """
    if not duration_s > 0.0:
        raise ValueError("Bad parameter: duration_s must be > 0.0")
    if not n_samples > 0:
        raise ValueError("Bad parameter: n_samples must be > 0.")

    # Corner case: going nowhere.
    if l_vel == 0:
        return null_trajectory(n_samples)

    # Path distance we'll travel in the given time, at the given speed.
    s = l_vel * duration_s

    # Radius of the trajectory.
    try:
        r = l_vel / a_vel
    except ZeroDivisionError:
        return straight_trajectory(l_vel, duration_s, n_samples)

    # Angle around the circle.
    theta = s / r

    # Samples the angle space.
    d_theta = np.linspace(0, theta, n_samples)

    # Generate the points.
    points = np.array([(r * np.cos(theta), r * np.sin(theta), 0.0)
                       for theta in d_theta])
    orientations = [xf.quaternion_from_euler(theta, 0.0, 0.0, 'rzyx')
                    for theta in d_theta]

    # We generated the points around the origin, starting at (r, 0). We want
    # the trajectory's starting point to be at the origin of our base_frame,
    # with the center of the circle on the y-axis. This can be accomplished
    # through the following transformations:
    # 1) A rotation through -90 degrees.
    # 2) A translation along the y-axis, of distance r.
    R = xf.euler_matrix(-np.pi / 2.0, 0.0, 0.0, 'rzyx')
    T = xf.translation_matrix([0.0, r, 0.0])
    homo_points = np.dot(np.dot(T, R), homo(points).T)

    # Back to row vectors.
    return homo_points.T[:, :3], orientations


def homo(xyz):
    """
    Return a mx4 np.array of homogeneous points from a mx3 array of 3D points.
    where m is the number of points in xyz.
    Args:
        xyz (np.array(mx3): list of 3D points, in (x, y, z) order.

    Returns:
        np.array(mx4).
    """
    return np.hstack((xyz, np.ones((len(xyz), 1))))


def make_path(points, orientations, timestamp, frame_id):
    """
    Return a nav_msgs/Path built from the args.

    Increment header.seq for the poses, starting at zero.
    Args:
        points (np.array): 3D positions.
        orientations (np.array): 4-tuples (quaternions).
        timestamp (rospy.Time): timestamp with which to mark the poses and path.
        frame_id (str): associated tf frame.

    Returns: a nav_msgs/Path, with all headers stamped with
    the passed-in timestamp and frame id.
    """
    poses = [Pose(Point(*p), Quaternion(*o))
             for p, o in zip(points, orientations)]
    poses_stamped = [PoseStamped(Header(n, timestamp, frame_id), p)
                     for n, p in enumerate(poses)]
    return Path(Header(stamp=timestamp, frame_id=frame_id), poses_stamped)


def min_samples(v, t, costmap_resolution):
    """
    Return the minimum number of sample points required for the given
    speed, duration, and costmap.

    Calculate the minimum number of points needed to predict the safety of our
    commanded velocity. We must sample densely-enough to check every
    grid cell in the costmap that intersects the arc of the proposed
    trajectory. We must choose a sampling density that is <= the costmap
    resolution (ala Nyquist). We choose 3X for good measure.

    Args:
        v (float): velocity, in meters per second.
        t (float): a time duration, in seconds.
        costmap_resolution (float): meters.

    Returns the minimum number of samples (int).

    """
    return int(ceil((fabs(v) * t) / (3 * costmap_resolution)))


def null_trajectory(n_samples):
    if not n_samples > 0:
        raise ValueError("n_samples must be > 0.")

    points = np.array([(0, 0, 0) for i in range(n_samples)])
    orientations = np.array([(1, 0, 0, 0) for i in range(n_samples)])
    return points, orientations


def pt_to_cell_idx(pt, grid_spacing):
    """
    Return a grid cell index that corresponds to pt, assuming the given
    grid_spacing.

    Indices are calculated in such a way as to be symmetrical around the origin,
    so that coordinates that fall exactly on cell index boundaries will be
    assigned to the larger cell index (in magnitude).

    For example, if the grid spacing is 0.1,
        0.2 -> 2
        0.15 -> 1
        0.1 -> 1
        0.05 -> 0
        0.0 -> 0
        -0.05 -> -1
        -0.1 -> -2
        -0.15 -> -2
        -0.2 -> -3

    cell indices:   -2   -1    0    1    2
                  |''''|''''|''''|''''|''''|
    coords:     -0.2 -0.1   0   0.1  0.2  0.3

    Args:
        pt (np.array): 1xn coordinate vector.
        grid_spacing (float): m/cell.

    Returns:
        list (int), list of cell indices.
    """
    assert grid_spacing > 0.0  # doesn't make sense otherwise

    cell = []
    for coord in pt:
        idx = int(coord / grid_spacing)

        # round up (in magnitude), to the next cell
        if coord < 0.0:
            idx -= 1

        cell.append(idx)
    return cell


def straight_trajectory(l_vel, duration_s, n_samples):
    if not duration_s > 0.0:
        raise ValueError("Bad parameter: duration_s must be > 0.0")
    if not n_samples > 0:
        raise ValueError("Bad parameter: n_samples must be > 0.")

    if l_vel == 0.0:
        return null_trajectory(n_samples)

    d = l_vel * duration_s
    x = np.linspace(0, d, n_samples)
    points = np.array([(x_i, 0, 0) for x_i in x])
    orientations = np.array([(1, 0, 0, 0) for i in range(n_samples)])
    return points, orientations


def transform_points(pts, trans, rot):
    """
    Perform a rotation, then translation, on a list of 3D points.
    Args:
        pts (list(np.array): mx3 row vectors, where m == len(pts).
        trans (3-tuple): 1x3 translation vector.
        rot (np.array): a quaternion ([w, x, y , z]).

    Returns:
        mx3 np.array of transformed points.
    """
    pts_h = homo(pts).T  # To homogeneous column vectors.
    t = xf.translation_matrix(trans)
    r = xf.quaternion_matrix(rot)
    xform = np.dot(t, r)
    pts_out = np.dot(xform, pts_h).T  # Back to row vectors.
    return pts_out[:, :3]
