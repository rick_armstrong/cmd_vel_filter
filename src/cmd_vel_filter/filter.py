# Copyright (c) 2019, Rick Armstrong
# All rights reserved.
#
# License:
# https://bitbucket.org/rick_armstrong/cmd_vel_filter/src/master/LICENSE
#
import numpy as np
import transformations as xf

from cmd_vel_filter.trajectory import pt_to_cell_idx, transform_points


def collision_is_imminent(trajectory, costmap_msg,  theta, debug_out=None):
    """
    Decide if there is a risk of collision.

    Given a costmap and a trajectory, decide if a collision is imminent. We'll
    use the the most-conservative measure: if the total cost is above zero,
    call it an imminent collision.
    Args:
        trajectory (list(3-tuple): a trajectory (points-only, no orientations),
        expressed in the base frame.
        costmap_msg (nav_msgs/OccupancyGrid): a ROS costmap message.
        theta (float): angle to which we should rotate the trajectory.
        debug_out (np.array): if not None, we assume that debug_out is a
          copy of costmap_msg.data we set grid cells that intersect the
          trajectory to 255. For debugging/visualization purposes.
    Returns: bool
    """
    # Unpack the message.
    width = costmap_msg.info.width
    height = costmap_msg.info.height
    resolution = costmap_msg.info.resolution
    costmap = np.array(costmap_msg.data, dtype='B').reshape((width, height))
    
    # Rotate the points to their correct orientation in the costmap.
    rot = xf.quaternion_from_euler(theta, 0.0, 0.0, 'rzyx')
    trajectory_orientated = transform_points(trajectory, (0.0, 0.0, 0.0), rot)

    # We assume a rolling costmap, so transform the 'gridded' trajectory to the
    # center of the costmap.
    cells = [pt_to_cell_idx(p, resolution) for p in trajectory_orientated]
    costmap_cells = map(lambda pt: (pt[0] + (width / 2),
                                    pt[1] + (height / 2)), cells)

    # Cull cell indices that are outside of our costmap.
    costmap_cells = [cell for cell in costmap_cells if not (cell[0] >= width)]
    costmap_cells = [cell for cell in costmap_cells if not (cell[0] >= height)]

    # If requested, make copy of the costmap data
    # and draw the gridded trajectory.
    if debug_out is not None:
        for p in costmap_cells:  # Draw the path.
            debug_out[p[1], p[0]] = 255

    costs = np.array([costmap[cell[1], cell[0]] for cell in costmap_cells])
    if np.sum(costs) > 0.0:
        return True
    return False
