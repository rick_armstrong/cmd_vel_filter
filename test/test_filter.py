# Copyright (c) 2019, Rick Armstrong
# All rights reserved.
#
# License:
# https://bitbucket.org/rick_armstrong/cmd_vel_filter/src/master/LICENSE
#
from math import fabs, ceil

import numpy as np
from PIL import Image
import pytest
import rosbag

from cmd_vel_filter.filter import collision_is_imminent
from cmd_vel_filter.trajectory import calc_trajectory, pt_to_cell_idx


@pytest.mark.parametrize("v,t,w,theta,crash",
                         [
                            # Straight-ahead: definitely a crash.
                            (1.0, 1.0, 0.0, 0.0, True),

                            # Straight, away from the obstacle: not a crash.
                            (1.0, 1.0, 0.0, -1.57, False),

                            # Backing up and turning a little: not a crash.
                            (-3.0, 1.0, 0.5, 0.0, False),
                         ])
def test_collision_is_imminent(
        test_data_dir,
        test_artifact_dir,
        v, t, w, theta, crash):
    # Load test message.
    bagpath = test_data_dir.join('one_costmap.bag')
    bag = rosbag.Bag(str(bagpath), 'r')
    topic, msg, stamp = bag.read_messages(
        topics=['/move_base/local_costmap/costmap']).next()

    # Create a trajectory from the test params.
    n = int(ceil((fabs(v) * t) / (3 * msg.info.resolution)))
    points, orientations = calc_trajectory(v, w, t, n)

    # Make a copy of the costmap data for visualization.
    debug = np.array(msg.data, dtype='B').reshape(
        (msg.info.width, msg.info.height))

    is_crash = collision_is_imminent(points, msg, theta, debug)

    # Show the intersection of the points with the costmap grid.
    img = Image.fromarray(debug)
    flipped = img.transpose(method=Image.TRANSVERSE)
    flipped.show()

    assert is_crash is crash
