# Copyright (c) 2019, Rick Armstrong
# All rights reserved.
#
# License:
# https://bitbucket.org/rick_armstrong/cmd_vel_filter/src/master/LICENSE
#
from nav_msgs.msg import Path
import numpy as np
import pytest
import rospy
import time

from cmd_vel_filter.trajectory import calc_trajectory, make_path, \
    straight_trajectory, transform_points

# For readability.
sin_45 = np.sin(np.pi / 4.0)


def test_calc_trajectory():
    # Quadrant I, forward, left-turning.
    v = np.pi / 2.0
    w = v
    t = 1.0
    n = 3
    points, orientations = calc_trajectory(v, w, t, n)
    assert np.allclose(points[0, :], np.array((0, 0, 0)))
    assert np.allclose(points[1, :], np.array((sin_45, -sin_45 + 1.0, 0)))
    assert np.allclose(points[2, :], np.array((1, 1, 0)))

    # Quadrant IV: forward, right-turning.
    v = np.pi / 2.0
    w = -v
    t = 1.0
    n = 3
    points, orientations = calc_trajectory(v, w, t, n)
    assert np.allclose(points[0, :], np.array((0, 0, 0)))
    assert np.allclose(points[1, :], np.array((sin_45, sin_45 - 1.0, 0)))
    assert np.allclose(points[2, :], np.array((1, -1, 0)))

    # Quadrant II: reverse, right-turning.
    v = -np.pi / 2.0
    w = v
    t = 1.0
    n = 3
    points, orientations = calc_trajectory(v, w, t, n)
    assert np.allclose(points[0, :], np.array((0, 0, 0)))
    assert np.allclose(points[1, :], np.array((-sin_45, -sin_45 + 1.0, 0)))
    assert np.allclose(points[2, :], np.array((-1, 1, 0)))

    # Quadrant II: reverse, left-turning.
    v = -np.pi / 2.0
    w = v
    t = 1.0
    n = 3
    points, orientations = calc_trajectory(v, w, t, n)
    assert np.allclose(points[0, :], np.array((0, 0, 0)))
    assert np.allclose(points[1, :], np.array((-sin_45, -sin_45 + 1.0, 0)))
    assert np.allclose(points[2, :], np.array((-1, 1, 0)))

    # Corner case: zero linear velocity, non-zero angular (pivoting left,
    # in-place). We expect all points to be at base_frame origin.
    v = 0.0
    w = np.pi / 2.0
    t = 1.0
    n = 3
    points, orientations = calc_trajectory(v, w, t, n)
    assert np.allclose(points, np.zeros((n, 3)))

    # Corner case: zero angular velocity, i.e, a straight line.
    v = np.pi / 2.0
    w = 0.0
    t = 1.0
    n = 3
    points, orientations = calc_trajectory(v, w, t, n)

    # Last point at the end of the line.
    assert np.allclose(points[-1], (v * t, 0, 0))

    # Orientations all-identity.
    assert np.allclose(orientations[:, 0], np.ones(n))
    assert np.allclose(orientations[:, 1:4], np.zeros((n, 3)))


def test_make_path():
    v = np.pi / 2.0
    w = 0.0
    t = 1.0
    n = 3
    timestamp = rospy.Time.from_sec(time.time())
    frame_id = 'base_link'
    points, orientations = calc_trajectory(v, w, t, n)
    p = make_path(points, orientations, timestamp, frame_id)
    assert type(p) == Path
    assert len(p.poses) == n


@pytest.mark.parametrize("v,t,n_samples",
                         [
                             (1.0, 1.0, 10),
                             (-1.0, 1.0, 100),
                             (0, 1.0, 1)
                         ])
def test_straight_trajectory(v, t, n_samples):
    points, orientations = straight_trajectory(v, t, n_samples)

    # First point at origin.
    assert np.allclose(points[0, :], np.array((0, 0, 0)))

    # Last point: end of the line.
    assert np.allclose(points[-1], (v * t, 0, 0))

    # Orientations all-identity.
    assert np.allclose(orientations[:, 0], np.ones(n_samples))
    assert np.allclose(orientations[:, 1:4], np.zeros((n_samples, 3)))


@pytest.mark.parametrize("trans,rot,pts_in,pts_out",
                         [
                             # Rotation-only, -90 degrees around z.
                             (
                                (0.0, 0.0, 0.0),
                                (0.707, 0.0, 0.0, -0.707),
                                [(0.0, 0.0, 0.0), (1.0, 0.0, 0.0)],
                                [(0.0, 0.0, 0.0), (0.0, -1.0, 0.0)]
                             ),

                             # Rotation, -90 degrees around z, then translation
                             # on the y-axis.
                             (
                                 (0.0, 1.0, 0.0),
                                 (0.707, 0.0, 0.0, -0.707),
                                 [(0.0, 0.0, 0.0), (1.0, 0.0, 0.0)],
                                 [(0.0, 1.0, 0.0), (0.0, 0.0, 0.0)]
                             ),

                             # Translation-only, along the -y-axis.
                             (
                                 (0.0, -1.0, 0.0),
                                 (1.0, 0.0, 0.0, 0.0),
                                 [(0.0, 0.0, 0.0), (1.0, 0.0, 0.0)],
                                 [(0.0, -1.0, 0.0), (1.0, -1.0, 0.0)]
                             ),

                         ])
def test_transform_points(trans, rot, pts_in, pts_out):
    """Simple checks to verify that we know what we're doing."""
    out = transform_points(pts_in, trans, rot)
    assert np.allclose(np.array(pts_out), np.array(out))
