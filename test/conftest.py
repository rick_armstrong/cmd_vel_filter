# Copyright (c) 2019, Rick Armstrong
# All rights reserved.
#
# License:
# https://bitbucket.org/rick_armstrong/cmd_vel_filter/src/master/LICENSE

import subprocess as sp

import py
import pytest
import rospy


@pytest.fixture(scope="session")
def roscore():
    print("Start ROS master.")
    master = sp.Popen("roscore")
    yield master
    print("Stop ROS master.")
    master.terminate()


@pytest.fixture(scope="module")
def node():
    rospy.init_node('test', anonymous=True)


@pytest.fixture(scope="session")
def test_artifact_dir():
    this_dir = py.path.local(__file__).dirpath()
    tad = this_dir.join('test_artifacts').ensure(dir=True)
    return tad


@pytest.fixture(scope="session")
def test_data_dir():
    this_dir = py.path.local(__file__).dirpath()
    tdd = this_dir.join('test_data').ensure(dir=True)
    return tdd

