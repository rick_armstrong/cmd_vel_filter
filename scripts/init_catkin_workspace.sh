#!/bin/bash
# First-time setup: create a new Catkin workspace, link this package, build.
pushd ../..
mkdir -p cmd_vel_filter_ws/src

pushd cmd_vel_filter_ws/src
ln -s ../../cmd_vel_filter cmd_vel_filter
popd

pushd cmd_vel_filter_ws
catkin_make
popd

popd
