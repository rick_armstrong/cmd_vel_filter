#!/usr/bin/env python2
"""
Become a ROS node, wait for a very specific costmap, then view it.
"""
from nav_msgs.msg import OccupancyGrid
import numpy as np
from PIL import Image
import rospy


if __name__ == '__main__':
    rospy.init_node('test', anonymous=True)
    msg = rospy.wait_for_message('/move_base/local_costmap/costmap',
                                 OccupancyGrid)
    costmap = np.array(msg.data, dtype='B').reshape((200, 200))
    img = Image.fromarray(costmap)

    # Image coordinates have a flipped y-axis, compared to the odom frame.
    flipped = img.transpose(method=Image.FLIP_TOP_BOTTOM)
    flipped.show()

